require(tidyverse)
require(lubridate)
require(magrittr)

to_markdown_list <- function(item_names, enumerate = F, tab = T) {
  if (enumerate) {
    item_names <- item_names %>% imap_chr(~ sprintf("%d. %s", .y, .x))
  } else {
    item_names <- str_c("- ", item_names)
  }
  
  if (tab) {
    item_names <- str_c("\t", item_names)
  }
  
  item_names %>% str_c(collapse = ",\n") %>% cat() 
}

## to be edited
kable_styled <- function(df, caption, first_italic = T, center_right_cols = F, percents = F, na_flag = NULL) {
  if(center_right_cols) {
    align_spec <- c("l", rep("c", ncol(df) - 1)) %>% str_c()
  } else {
    align_spec <- "l"
  }
  
  df <- df %>% mutate_if(is.numeric, ~ round(.x, 2))
  
  if(percents) {
    df <- df %>% mutate_if(is.numeric, ~ ifelse(
      is.na(.x),
      as.character(NA),
      format(.x, digits = 2, nsmall = 2) %>% sprintf("%s%%", .)
    ))
  }
  
  if(!is.null(na_flag)) {
    df <- df %>% mutate_all(~ as.character(.x) %>% ifelse(is.na(.), na_flag, .))
  }
  
  df %>% 
    kable(digits = 2, format = "latex", caption = caption, align = align_spec) %>% 
    kable_styling(position = "center", latex_options = "HOLD_position") %>%
    row_spec(0, bold = TRUE, background = "#ffd966") %>%
    column_spec(1, italic = first_italic, border_left = T) %>%
    column_spec(ncol(df), border_right = T)
}

obtain_lm_formula <- function(df, response, powers = 2:2, interactions = TRUE, log_response = FALSE, intercept = TRUE) {
  vars <- colnames(df) |> keep(~ .x != response)
  form <- reformulate(vars, response = response)
  
  if (interactions) {
    form <- form |> update(. ~ (.)^2)
  }
  
  if (length(powers) > 0L) {
    vars_num <- df |> 
      select(where(is.numeric), -!!rlang::sym(response)) |>
      colnames()
    
    vars_powers <- powers |> map(
      ~ sprintf("I(%s^%i)", vars_num, .x)
    ) |> unlist()
    
    form <- form |> update(
      reformulate(c(".", vars_powers), ".")
    )
  }
  
  if (log_response) {
    form <- form |> update(log(.) ~ .)
  }
  
  form
}

lm_stepwise <- function(formula, df, model = NULL) {
  if (is.null(model)) {
    model <- lm(formula, data = df)
  }
  
  df_train <<- df ## temporary workaround, probably there's a bug inside MASS::stepAIC
  
  MASS::stepAIC(
    model,
    scope = list(
      lower = ~ 1,
      upper = formula |> delete.response()
    ),
    direction = "both",
    trace = 0
  )
}

mape <- function(y, y_hat) {
  100 * mean(abs((y - y_hat) / y))
}

lm_predict_eval <- function(model, target_var, new_data, log_target = F, standarise = F) {
  true_target <- new_data[[target_var]]
  
  if (standarise) {
    mean_target <- mean(true_target)
    sd_target <- sd(true_target)
      
    new_data <- new_data |> mutate_if(is.numeric, ~ as.numeric(scale(.x)))
  }
  
  predictions <- model |> predict(new_data)
  
  if (log_target) {
    predictions <- exp(predictions)
  }
  
  if (standarise) {
    predictions <- predictions * sd_target + mean_target
  }

  tibble(
    `adj. R2` = summary(model)$r.squared,
    `MAPE in-sample` = mape(fitted(model) + residuals(model), fitted(model)),
    `MAPE out-of-sample` = mape(true_target, predictions)
  )
}

lm_test <- function(model) {
  list(
    `Jarque-Bera` = tseries::jarque.bera.test(model$residuals),
    `Shapiro-Wilk` = shapiro.test(model$residuals),
    `Ramsey RESET` = lmtest::resettest(model),
    `Breush-Pagan` = lmtest::bptest(model)
  ) |> 
    map_dfc(~ as.numeric(.x[c("statistic", "p.value")])) |> 
    set_rownames(c("statistic", "p-value"))
}

max_vif <- function(model) {
  raw_vif <- car::vif(model)
  
  if (is.matrix(raw_vif)) {
    vif_table <- raw_vif |>
      as_tibble(rownames = NA) |>
      rownames_to_column("variable") |>
      mutate(VIF = `GVIF^(1/(2*Df))`^2) |>
      dplyr::select(variable, VIF) |>
      arrange(-VIF) |>
      head(n = 5)
  } else {
    vif_table <- tibble(
      variable = names(raw_vif),
      VIF = as.numeric(raw_vif)
    )  
  }
  
  vif_table
}

kable_default <- function(df, caption, print_row_names = F, first_italic = T, center_right_cols = T, digits = 2, label = NA) {
  if (center_right_cols) {
    if (print_row_names) {
      align_spec <- rep("c", ncol(df) - 1) |> str_c()
    } else {
      align_spec <- c("l", rep("c", ncol(df) - 1)) |> str_c()
    }
  } else {
    align_spec <- "l"
  }
  
  col_number <- ifelse(print_row_names, 1, 0) + ncol(df)
  
  df |>
    kable(digits = digits, format = "latex", caption = caption, align = align_spec, row.names = print_row_names, label = label) %>% 
    kable_styling(position = "center", latex_options = "HOLD_position") %>%
    row_spec(0, bold = TRUE) %>%
    column_spec(1, italic = first_italic, border_left = T) %>%
    column_spec(col_number, border_right = T)
}

remove_unsignificant_vars <- function(model) {
  coefs <- summary(model)$coefficients[, "Pr(>|t|)"]
  sig_coef_names <- names(coefs[coefs <= 0.1]) |>
    discard(~ .x == "(Intercept)")
  
  df <- model$model |> 
    as_tibble() |> 
    select(!starts_with("I("))
  
  log_column_names <- df |>
    select(starts_with("log(")) |>
    colnames()
  
  df <- df |>
    mutate(across(
      all_of(log_column_names),
      exp
    )) |>
    rename_all(
      ~ .x |>
        str_remove("^log\\(") |>
        str_remove("\\)$")
    )
  
  factor_vars_name_mapping <- df |> 
    select(where(is.character)) |>
    imap_chr(
      ~ str_c(.y, unique(.x)) %>% 
        sprintf("(%s)", .) |> 
        str_c(collapse = "|")
    )
  
  factor_vars_name_mapping |> iwalk(~ {
    sig_coef_names <<- sig_coef_names |> str_replace(.x, .y)
  })
  
  sig_coef_names <- unique(sig_coef_names)
  
  new_formula <- model$call$formula |> update(reformulate(sig_coef_names, response = "."))
  
  lm(new_formula, data = df)
}

plot_residuls <- function(model) {
  df_model <- tibble(
    ehat = residuals(model),
    fit = fitted(model)
  )
  
  plt_histogram <- ggplot(df_model, aes(x = ehat)) +
    geom_histogram(aes(y = ..density..), colour = "black", fill = "grey", bins = 15) + 
    labs(title = "Empirical residuals vs Gaussian curve", x = NULL) +
    stat_function(
      fun = dnorm,
      args = list(
        mean = mean(df_model$ehat),
        sd = sd(df_model$ehat)
      ),
      col = "red",
      size = 1
    ) +
    theme(plot.title = element_text(hjust = 0.5))
  
  plt_residuals <- ggplot(df_model, aes(x = fit, y = ehat)) + 
    geom_point() +
    labs(title = "Residuals vs fitted values", x = "fitted values", y = "residuals") +
    theme(plot.title = element_text(hjust = 0.5))
  
  gridExtra::marrangeGrob(list(plt_histogram, plt_residuals), nrow = 1, ncol = 2, top = NULL)
}

summary_tibble <- function(df) {
  df |> 
    map(summary) %>% 
    do.call(cbind, .) |> 
    as_tibble(rownames = NA)
}


